
public class OperadorTernario {

    public static void main(String[] args) {
        
        int hora = 15;
        
        String result = hora < 12 ? "Bom dia" : "Boa tarde";
        
        System.out.println(result);
    }
}
